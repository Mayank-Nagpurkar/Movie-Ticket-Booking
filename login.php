<!DOCTYPE HTML>
<html>
<head>
    <title>Movie Booking</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Movie Booking</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
        </ul>
    </div>
</nav>
<div class="content">
  <div class="wrap">
    <div class="content-top">
        <div class="page-not-found">
        <div class="pnot">
            <div id="login">
              <h2>Login</h2>
              <form action = "process.php" method="post" class="form">
                <p class="required">* required fields </p><br>
                <p>
                  <label>Userid : </label><br>
                  <input type = "text" name= "username" required>&nbsp;<span class = "required">*</span><br><br>
                </p>
                <p>
                  <label>Password :  </label><br>
                  <input type = "password" name= "password" required>&nbsp;<span class = "required">*</span><br><br>
                </p>
                <input class = "btn" type = "submit" value="Login"><br>
                <p>New User ? <a href = "Signup.php">Sign Up Now!</a></p>
              </form>

            </div>
        </div>
      </div>
        <div class="clear"></div>
      </div>
  </div>
</div>
<div class="content">
</div>
<footer id="myFooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2 class="logo"><a href="#">MOVIES</a></h2>
            </div>
            <div class="col-sm-2">
                <h5>Get started</h5>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Downloads</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5>About us</h5>
                <ul>
                    <li><a href="#">Company Information</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li><a href="#">Reviews</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5>Support</h5>
                <ul>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Help desk</a></li>
                    <li><a href="#">Forums</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <div class="social-networks">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>© 2016</p>
    </div>
</footer>
</body>
</html>
