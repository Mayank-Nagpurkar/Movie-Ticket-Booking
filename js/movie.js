function movieDelete(id) {
	if(confirm("Do you really want to delete this movie's data?")) {
			
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					//alert(this.responseText);
					response = JSON.parse(this.responseText);
					
					if(response["Success"] == "True") {
						//alert("Movie Record deleted Successfully!");
						var row = document.getElementById(id);
						row.parentNode.removeChild(row)
					} else {
						alert("Delete failed!");
					}
				}
			}
			//console.log(id);
			//console.log(typeof(id));
			xhttp.open("POST", "movief.php", true); // asynchronous
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.send("reqType=movieDelete&movieId=" + id);
		}
}

function showDelete(id) {
	if(confirm("Do you really want to delete this show's data?")) {
			
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					//alert(this.responseText);
					response = JSON.parse(this.responseText);
					
					if(response["Success"] == "True") {
						alert("Show Record deleted Successfully!");
						var row = document.getElementById(id);
						row.parentNode.removeChild(row)
					} else {
						alert("Delete failed!");
					}
				}
			}
			//alert(id);
			xhttp.open("POST", "movief.php", true); // asynchronous
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.send("reqType=showDelete&showId=" + id);
		}
}

function checkMovie() {
	console.log("i can checking");
	var Mname = document.getElementById('mname').value;
	var rdate = document.getElementById('rdate').value;
	var dire = document.getElementById('dire').value;
	var act = document.getElementById('act').value;
	
	
	if(!(/^[A-Za-z\s]+$/.test(Mname))) {
        alert("Movie Name Field is not valid. Special characters not allowed");
        return false;
    }
}
