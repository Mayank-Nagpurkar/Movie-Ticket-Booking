<?php
require_once ("header.php")?>
<div class="content">
    <div class="wrap">
        <div class="content-top">
            <div class="page-not-found">
                <div class="pnot">
                    <div id = "login">
                        <h2>Book Movie</h2>
                        <form action= "selector.php" method="post" class="form">
							<?php
							require_once('db.php');
							$movie_id = $_POST['movie_id'];

                            $conn = dbConnect();
							$sql = "SELECT tid FROM shows where movie_id = '$movie_id'";
							$result = $conn->query($sql);
							if($result === false) {
								die("Incorrect tid check again");
							}
							$row = $result->fetch_assoc();
							$tid = $row["tid"];
							?>	
                            <p class="required">* required fields </p><br>
                            <p>
                            <p>
                                <label>Theatre name : </label><br>
                                <?php

                                $sql = "SELECT tname FROM theatre where tid = '$tid'";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    echo "<select name = \"tname\">";
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value = \"".htmlspecialchars($row['tname'])."\"> " . $row["tname"]. "<br>";
                                    }
                                    echo "</select>";
                                }
                                else {
                                    echo "0 results";
                                }
                                $conn->close();
                                ?>
                            </p><br>
                            <p>
                            <p>
                                <label>Show Date : </label><br>
                                <input type = "date" name= "show_date" required>&nbsp;<span class = "required">*</span><br><br>
                            </p>
                            <label>Show time : </label><br>
                            <select name = "show_time">
                                <option value="1">9am-12pm</option>
                                <option value="2">12noon-3pm</option>
                                <option value="3">3pm-6pm</option>
                                <option value="4">6pm-9pm</option>
                                <option value="5">9pm-12am</option>
                            </select>&nbsp;<span class = "required">*</span><br><br>
                            </p>
                            <p>
                                <label>Screen :  </label><br>
                                <select name = "screen_id">
                                    <option value="1">1</option>
                                </select>&nbsp;<span class = "required">*</span><br><br>
                            </p>
                            <input type="hidden" name="movie_id" value="<?php echo $movie_id;?>">
                            <input class = "btn" type = "submit" value="Select Seats"><br>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php require_once ("footer.php");
?>
