<?php
require_once('config.php');

//connect to database

global $CFG;
$con = mysqli_connect($CFG->server, $CFG->db_user, $CFG->db_pass);

function dbConnect() {
	global $CFG;
	$conn = new mysqli($CFG->server, $CFG->db_user, $CFG->db_pass, $CFG->db_database);
	if($conn->error) {
		die("connection error ". $conn->error . "<br>"); 
		return false;
	}
	$CFG->conn = $conn;
	return $conn;
}
if($con === false){
    die("ERROR: Could not connect to mysql server. " . mysqli_connect_error());
}
$database = mysqli_select_db($con, $CFG->db_database);
if(!$database){
    $create = "CREATE DATABASE movie";
	if (mysqli_query($con, $create)) {
    	echo "Database created successfully<br>";
	}
	else {
    	echo "Error creating database: " . mysqli_error($conn);
    }

	$conn  = dbConnect();
	$filename = 'movie.sql'; 
	$op_data = '';
	$lines = file($filename);
	foreach ($lines as $line)
	{
		if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
		{
			continue;
		}
		$op_data .= $line;
		if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
		{
			$conn->query($op_data);
			$op_data = '';
		}
	}

}

function dbConnectNoDatabase() {
	global $CFG;
	$conn = new mysqli($CFG->server, $CFG->db_user, $CFG->db_pass);
	if($conn->connect_error) {
		return false;
	}
	$CFG->conn = $conn;
	return $conn;
}




//fetch all rows of a table from database
function sqlGetAllRows($query) {
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false)  {
		$CFG->last_query = $query;
		die("Query $query returned false");
	}
	$allrows = array();
	$allrows = $result->fetch_all(MYSQLI_ASSOC);
	return $allrows;
}

//get one row from database
function sqlGetOneRow($query) {
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false)  {
		die("sqlGetOneRow: Query $query returned false");
	}
	$allrows = array();
	$allrows = $result->fetch_all(MYSQLI_ASSOC);
	if(count($allrows) != 1) {
		$CFG->last_query = $query;
		die("sqlGetOneRow: $query returned ".count($allrows)." rows");
	}
	return $allrows;
}

//update rows
function sqlUpdate($query) {
	global $CFG;
	if($CFG->conn === false)
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false) {
		$CFG->last_query = $query;	
		return false;
	}
	return true;
}

?>