<?php

require_once('db.php');

$mname = $_POST['moviename'];
$reldate = $_POST['reldate'];
$director = $_POST['director'];
$actor = $_POST['actor'];
$genre = $_POST['genre'];
$certification = $_POST['cert'];
$description = $_POST['description'];


//vallidate file name

$input_name = trim($_POST["moviename"]);
    if(empty($input_name)){
        $name_err = "Please enter a movie name.";
    } elseif(!filter_var(trim($_POST["moviename"]), FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z'-.\s ]+$/")))){
        $name_err = 'Please enter a valid movie name.';
    } else{
        $mname = $input_name;
    }
	
	$input_name = trim($_POST["director"]);
    if(empty($input_name)){
        $direct_err = "Please enter a director name.";
    } elseif(!filter_var(trim($_POST["director"]), FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z'-.\s ]+$/")))){
        $direct_err = 'Please enter a valid director name';
    } else{
        $director = $input_name;
    }
	
	$input_name = trim($_POST["actor"]);
    if(empty($input_name)){
        $act_err = "Please enter a actors name.";
    } elseif(!filter_var(trim($_POST["actor"]), FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z'-.\s ]+$/")))){
        $act_err = 'Please enter a valid actor name.';
    } else{
        $actor = $input_name;
    }
	
	$input_year = trim($_POST["reldate"]);
    if(empty($input_year)){
        $year_err = 'Please enter correct release date of movie';     
    }
    elseif(!ctype_digit($input_year)) {
         $year_err = 'Please enter a positive integer value.';
    }
    else{
        if($input_year < date("m/d/Y"))
            $year_err = 'Error in release date';
        $year = $input_year;
    }
	




$fileName = $_FILES['Filename']['name'];

$target = 'uploads/';	
$fileTarget = $target.$fileName;
//echo $fileTarget;
$tempFileName = $_FILES["Filename"]["tmp_name"];
$result = move_uploaded_file($tempFileName,$fileTarget);

$con = dbConnect();

$fileTarget = mysqli_real_escape_string($con,$fileTarget);


if(empty($name_err) && empty($direct_err) && empty($act_err)) {
	$query = "insert into movie(movie_name,release_date,director,actors,genre,certification,description,cover) 
			values ('$mname', '$reldate', '$director', '$actor', '$genre', '$certification', '$description','$fileTarget')";

	$result = sqlUpdate($query);

	if($result === false) {
			 echo "{\"Success\": \"False\"}";
			 header("Location: admin_movie.php");
		}
	else {
			header("Location: admin_moviedel.php");
	}

}
else {
		echo "Error in movie data plz enter valid data again";
		header("Location: admin_movie.php");
	}		

?>
