<?php
require_once("admin_header.php");
?>
<div id = "tab-1">
	<div class="wrap">
		<div class="content-top">
				<div class="page-not-found">
				<div class="pnot">
          <div id="addshow">
	<table class="data-table">
		<caption class="title"><h3 align ="center">MOVIES</h3></caption>
		<thead>
			<tr>
				<th>MOVIE ID</th>
				<th>MOVIE NAME</th>
				<th>RELEASE DATE</th>
				<th>DIRECTOR</th>
				<th>ACTOR </th>
				<th>GENRE</th>
				<th>CERTIFICATION</th>
				<th>DESCRIPTION</th>
				<th>THUMBNAIL</th>
				<th>DELETE</th>
			</tr>
		</thead>
		<tbody>
		<?php
		require_once('db.php');
			$sql = "SELECT * FROM movie";
			$result = sqlGetAllRows($sql);
			if ($result === false) {
    		// output data of each row
				echo "0 results";
       		}
		foreach($result as $row)
		{
			$rid = $row['movie_id'];
			echo '<tr id=\''. $rid. '\'>
					<td>'.$row['movie_id'].'</td>
					<td>'.$row['movie_name'].'</td>
					<td>'.$row['release_date'].'</td>
					<td>'.$row['director'].'</td>
					<td>'.$row['actors'].'</td>
					<td>'.$row['genre'].'</td>
					<td>'.$row['certification'].'</td>
					<td>'.$row['description'].'</td>
					<td>'.$row['cover'].'</td>
					<td> <button type=\'submit\' name=\'submit\' value=\'dsubmit\' onclick=movieDelete(\''.$rid.'\')>Delete</button></td>
				</tr>';

		}
		?>
		</tbody>
	</table>
          </div>
		 		</div>
			</div>
				<div class="clear"></div>
			</div>
	</div>
</div>
<?php
require_once("footer.php");
?>
