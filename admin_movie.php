<?php
require_once("admin_header.php");
?>
<div class="content-1">
	<div class="wrap">
		<div class="content-top">
				<div class="page-not-found">
				<div class="pnot">
          <div id="addmovie">
            <h2><b>Add Movie</b></h2>
            <form action= "admin_addmovie.php" method="post" class="form"  enctype="multipart/form-data">
              <p class="required">* required fields </p>
              <p>
                <label>Movie Name </label><br>
                <input type = "text" id = "mname" name= "moviename" required>&nbsp;<span class=" required">*</span><br><br>
              </p>
              <p>
                 <label>Release Date</label><br>
                 <input type = "date" id = "rdate" name= "reldate" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Director </label><br>
                <input type = "text" id = "dire" name= "director" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Actor</label><br>
                <input type = "text" id = "act" name = "actor" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Certification</label><br>
                <select name = "cert" >
				<option value="U">U</option>
                <option value="U/A">U/A</option>
                <option value="A">A</option>
				</select>&nbsp;<span class = "required">*</span><br><br>
              </p>
             <p>
                <label>Genre</label><br>
                <select name = "genre" >
				<option value="Fiction">Fiction</option>
                <option value="Comedy">Comedy</option>
                <option value="Romantic">Romantic</option>
				<option value="Documentary">Documentary</option>
				</select>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Description</label><br>
                <input type = "text" name = "description" required>&nbsp;<span class = "required">*</span><br><br>
              </p> <p>
                <label>Cover</label><br>
                <input type = "file" name = "Filename" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <input class = "btn" type = "submit" value="Add Movie"><br>
            </form>

          </div>
		 		</div>
			</div>
				<div class="clear"></div>
			</div>
	</div>
</div>
<?php
require_once("footer.php");
?>
