<?php
session_start();


require_once ('TCPDF-master/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    public function Header() {
        // Logo
        $image_file = 'images/mlogo.jpg';
        $this->Image($image_file, 20, 10, 25, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, 'Movie Ticket Booking System', 0, false, 'C', 0, '', 0, false, 'M', 'M');

    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);



//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Jay');
$pdf->SetAuthor('MTBS Admin');
$pdf->SetTitle('MTBS E-Ticket');
$pdf->SetSubject('E-Ticket');

// set default header data
//$pdf->SetHeaderData('coep_logo.png',19, 'College of Engineering, Pune', 'An autonomous institute of Government of maharashtra');


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 20/* PDF_FONT_SIZE_MAIN*/));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

//$pdf->SetHeaderData('',19, '', 'An Autonomous institute of Government of Maharashtra');
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



// add a page
$pdf->AddPage();


$pdf->SetFont('helvetica', 'B', 10);

$txt = <<<EOD
E-Ticket Generator-Movie Ticket Booking System
EOD;


// print a block of text using Write()
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

$txt = "\n\n\n\n";
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

$txt = "<hr>";

$pdf->writeHTML($txt, true, false, false, false, '');


$pdf->SetFont('helvetica', 'B', 20);

$pdf->Write(0, 'E-Ticket', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('dejavusans', '', 15);

$html ="";
$pdf->writeHTML($html, true, false, false, false, '');


//$pdf->Image('images/barcode.png', '', '', 50, 50, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

$pdf->SetFont('dejavusans', '', 10);

$html .= "<p><b>Username : </b>".$_SESSION['username']."</p>";
$html .= "<p><b>Movie Name : </b>".$_SESSION['movie_name']."</p>";
$html .= "<p><b>Theatre Name : </b>".$_SESSION['theatre']."</p>";;
$html .= "<p><b>Screen id : </b>".$_SESSION['screen_id']."</p>";
$html .= "<p><b>Ticket Number : </b>".$_SESSION['ticket_no']."</p>";
$html .= "<p><b>Show Date :</b> ".$_SESSION['show_date']."</p>";
$html .= "<p><b>Show Time :</b> ".$_SESSION['show_time']."</p>";
$html .= "<p><b>Price :</b> ".$_SESSION['price']."</p><br><br>";
$booked = $_SESSION['booked'];
$count = count($booked);
$i = 0;
if($count > 0) {
    $html .= "Your Booked Seats are :";
    while ($count--) {
        if($booked[$i] < 10) {
            $html .=  'A-' .$booked[$i]%10 . ' ';
        }
        else {
            $html .= ' ' . chr(65 + ($booked[$i]/10)) . '-' . $booked[$i]%10 . ' ';
        }
        $i++;
    }
}


// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->Image('images/barcode.png', '', '', 150, 30, '', '', 'T', false, 300, '', false, false, 1, false, false, false);


$pdf->Output('eticket.pdf', 'I');

?>
