<?php
require_once ("header.php");
$movie_id = $_GET['movie_id'];
require_once ("db.php");
$conn = dbConnect();

$query = "SELECT * FROM movie WHERE movie_id = '$movie_id'";
$result = $conn->query($query);
if(!$result){
    echo "Can't retrieve data " . mysqli_error($conn);
    exit;
}

$row = mysqli_fetch_assoc($result);
if(!$row){
    echo "Empty Movies";
    exit;
}
$movie_name = $row['movie_name'];
?>
<p class="lead" style="margin: 25px 0"><a href="index.php">Movie</a> > <?php echo $row['movie_name']; ?></p>
<div class="row">
    <div class="col-md-3 text-center">
        <img class="img-responsive img-thumbnail" src="<?php echo $row['cover']; ?>">
    </div>
    <div class="col-md-6">
        <h4>Movie Description</h4>
        <p><?php echo $row['description']; ?></p>
        <h4>Movie Details</h4>
        <table class="table">
            <?php foreach($row as $key => $value) {
                if($key == "cover") {
                    continue;
                }
                switch($key){
                    case "release_date":
                        $key = "Release Date";
                        break;
                    case "director":
                        $key = "Director";
                        break;
                    case "certification":
                        $key = "Certification";
                        break;
                    case "genre":
                        $key = "Genre";
                        break;
                    case "actor";
                        $key = "Actor";
                        break;
                    case "end_date";
                        $key = "End Date";
                        break;
                }
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php echo $value; ?></td>
                </tr>
                <?php
            }
            if(isset($conn)) {mysqli_close($conn); }
            ?>
        </table>
        <form method="post" action="moviebook.php">
            <input type="hidden" name="movie_id" value="<?php echo $movie_id;?>">
            <input type="submit" value="Book Now" name="cart" class="btn btn-primary">
        </form>
    </div>
</div>
</body>
</html>
<?php
require_once("footer.php");