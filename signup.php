<!DOCTYPE HTML>
<html>
<head>
    <title>Movie Booking</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Movie Booking</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
        </ul>
    </div>
</nav>
<div class="content">
  <div class="wrap">
    <div class="content-top">
        <div class="page-not-found">
        <div class="pnot">
          <div id="login">
            <h2>Sign Up</h2>
            <form action= "adduser.php" method="post" class="form">
              <p class="required">* required fields </p>
              <p>
                <label>First Name : </label><br>
                <input type = "text" name= "firstname" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Last name : </label><br>
                <input type = "text" name= "lastname">&nbsp;<br><br>
              </p>
              <p>
                <p>
                  <label>Email : </label><br>
                  <input type = "email" name= "email" required>&nbsp;<span class = "required">*</span><br><br>
                </p>
                <label>Username : </label><br>
                <input type = "text" name= "username" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Set a Password :  </label><br>
                <input type = "password" name = "set_password" id = "setpassword" required>&nbsp;<span class = "required">*</span><br><br>
              </p>
              <p>
                <label>Confirm Password :  </label><br>
                <input type = "password" name = "password" id = "confirmpassword" required oninput="check(this)">&nbsp;<span class = "required">*</span><br><br>
              </p>
              <script>
              function check(input) {
                if (input.value != document.getElementById('setpassword').value) {
                   input.setCustomValidity('Password Must be Matching.');
                } else {
                 input.setCustomValidity('');
               }
              }
              </script>
              <input class = "btn" type = "submit" value="Sign up"><br>
              <p>Already a member? <a href = "login.php">Login</a></p>
            </form>

          </div>
        </div>
      </div>
        <div class="clear"></div>
      </div>
  </div>
</div>
</body>
</html>
<?php require_once ('footer.php');
?>