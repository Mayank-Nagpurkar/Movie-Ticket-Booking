<!DOCTYPE html>
<html lang="en">

<head>
    <title>Movie Booking</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src = "js/movie.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Movie Booking</a>
        </div>
        <ul class="nav navbar-nav">
			<li><a href="admin_movie.php">Add Movie</a></li>
			<li><a href="admin_show.php">Add Show</a></li>
			<li><a href="admin_price.php">Price</a></li>
			<li><a href="admin_bookings.php">Theater</a></li>
			<li><a href="admin_moviedel.php">Movies</a></li>
			<li><a href="admin_showss.php">Shows</a></li>
			<li><a href="admin_users.php">Users</a>
			<li><a href="login.php" class = "pull-right">LogOut</a>
        </ul>
    </div>
</nav>
