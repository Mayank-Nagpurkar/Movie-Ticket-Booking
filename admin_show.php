<?php
require_once("admin_header.php");
?>
<div class="content-1">
	<div class="wrap">
		<div class="content-top">
				<div class="page-not-found">
				<div class="pnot">
          <div id="addshow">
            <h2><b>Add Show</b></h2>
            <form action= "admin_addshow.php" method="post" class="form">
              <p class="required">* required fields </p>
              <label>Show time : </label><br>
					<select name = "show_time">
						<option value="1">9am-12pm</option>
						<option value="2">12noon-3pm</option>
						<option value="3">3pm-6pm</option>
						<option value="4">6pm-9pm</option>
						<option value="5">9pm-12am</option>
					</select>&nbsp;<span class = "required">*</span><br><br>
					</p>
              <p>
                <label>Language</label><br>
                <select name = "language">
						<option value="Hindi">Hindi</option>
						<option value="English">English</option>
						<option value="Other Regional">Others</option>
					</select>&nbsp;<span class = "required">*</span><br><br>
					</p>
              <p>
			  
                <label>Theatre Name</label><br>
                <?php
                                require_once("db.php");

                                $conn = dbConnect();


                                $sql = "SELECT tname FROM theatre";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    echo "<select name = \"tname\">";
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value = \"".htmlspecialchars($row['tname'])."\"> " . $row["tname"]. "<br>";
                                    }
                                    echo "</select>";
                                }
                                else {
                                    echo "0 results";
                                }
                                $conn->close();
                                ?>
              </p>
              <p>
                <label>Movie Name</label><br>
				<?php
								$conn = dbConnect();
                                $sql = "SELECT movie_name FROM movie";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    echo "<select name = \"movie_name\">";
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value = \"".htmlspecialchars($row['movie_name'])."\"> " . $row["movie_name"]. "<br>";
                                    }
                                    echo "</select>";
                                }
                                else {
                                    echo "0 results";
                                }
                                $conn->close();
                                ?>
              </p>
              <p>
                <label>Screen ID</label><br>
               <select name = "screen_ID">
						<option value="1">1</option>
              </select>&nbsp;<span class = "required">*</span><br><br>
				</p>
              <input class = "btn" type = "submit" value="Add Show"><br>
            </form>

          </div>
		 		</div>
			</div>
				<div class="clear"></div>
			</div>
	</div>
</div>
<?php
require_once("footer.php");
?>
