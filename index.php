<?php require_once ("header.php");
?>
<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/900/1.jpg">
      </div>

      <div class="item">
        <img src="images/900/2.jpg">
      </div>
    
      <div class="item">
        <img src="images/900/3.jpg">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div>
	<?php 
	require_once("db.php");
	$count = 0;
	$conn = dbConnect();
	$query = "SELECT movie_id,cover FROM movie";
  $result = $conn->query($query);
  if(!$result){
    echo "Can't retrieve data " . mysqli_error($conn);
    exit;
  }?>
	<p class="lead text-center text-muted" style="margin-top:100px">Now showing: </p>
    <div  id = "gallery">
    <?php for($i = 0; $i < mysqli_num_rows($result); $i++){ ?>
      <div class="row">
        <?php while($query_row = mysqli_fetch_assoc($result)){ ?>
          <div class="col-md-3">
            <a href="moviess.php?movie_id=<?php echo $query_row['movie_id']; ?>">
              <img class="img-responsive img-thumbnail" src="<?php echo $query_row['cover']; ?>">
            </a>
          </div>
        <?php
          $count++;
          if($count >= 4){
              $count = 0;
              break;
            }
          }}
           ?>
      </div>
    </div>
</div>
<?php
	require_once('footer.php');
?>